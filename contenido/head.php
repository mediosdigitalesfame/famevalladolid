<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Toyota® FAME Valladolid. Información detallada de Autos, SUV's, Minivans y Comerciales, agenda tu cita de servicio, prueba de manejo y cotizaciones. Piensa en auto, piensa en FAME.">
    <meta name="keywords" content="fame toyota morelia, fame toyota valladolid, toyota morelia, toyota valladolid, toyota michoacan, toyota fame, fame toyota, grupo fame toyota, fame, Grupo Fame, toyota acueducto, toyota agencia morelia, toyota morelia telefono, toyota morelia domicilio, toyota valladolid morelia, agencia toyota morelia, concesionatio toyota morelia, concesionario toyota, vendo toyota morelia, autos toyota morelia, morelia seminuevos toyota, morelia seminuevos, toyota en morelia, toyota morelia horario, toyota valladolid horario, toyota valladolid ubicacion, feme morelia, toyota fame valladolid, agencia toyota morelia michoacan, toyora usados en morelia, toyota morelia seminuevos, autos nuevos toyota, autos seminuevos toyota, autos agencia toyota morelia, toyota morelia refacciones, venta toyota morelia, mapa toyota morelia, toyota sienna, toyota sienna morelia, toyota avanza, toyota avanza morelia, toyota camry, toyota camry morelia, toyota hiace morelia, toyota hiaze, toyota combi morelia, toyota highlander, toyota highlander morelia, toyota hilux, toyota hilux morelia, toyota land cruiser, toyota land cruiser morelia, toyota prius, toyota prius morelia, toyota rav4, toyota rav4 morelia, toyota rav4 valladolid, toyota rav4 2015, toyota sequoia, toyota sequoia morelia, toyota tacoma, toyota tacoma morelia, toyota tundra, toyota tundra morelia, toyota yaris, toyota yaris morelia, toyota yaris sedan, toyota yaris sedan morelia, toyota corolla, toyota corolla morelia, toyota corolla 2015, agencia toyota morelia servicio, toyota servicio morelia, toyota hojalateria morelia, toyota garantia morelia, toyota cita de servicio morelia, toyota refacciones morelia, toyota promociones, toyota promociones morelia, toyota promociones valladolid, toyota seminuevos morelia, toyota seminuevos valladolid, combi toyota, numero toyota, telefono toyota, combi morelia, toyota 2018, nuevo yaris, nuevo yaris r, yaris 2018, tacoma 2018, nueva tacoma 2018, nueva hilux, nueva hilux 2018, hilux 2018">
    <meta name="author" content="Grupo FAME División Automotriz">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="icon" type="image/png" href="/images/favicon.png" />
    
    <!-- Inserta esta etiqueta en la sección "head" o justo antes de la etiqueta "body" de cierre. -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es-419'}
  </script>
